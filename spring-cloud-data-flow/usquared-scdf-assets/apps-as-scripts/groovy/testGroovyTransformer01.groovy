import groovy.json.JsonOutput;
import groovy.json.JsonSlurper;


class ShopTransaction {
	Long id
	Integer consumer_id
	String transaction_time
	Integer product_id
	BigDecimal unit_price
    Integer quantity
    Double advert
    Double promotion
}

println "========> I'm in 'testGroovyTransformer01' :: This the Payload Object = " + payload + " - Type: " + payload.getClass().getName();

def jsonSlurper = new JsonSlurper()

def oneShopTrans = jsonSlurper.parseText(payload) as ShopTransaction

// println "========> I'm in 'testGroovyTransformer01' :: ShopTransaction Object created from #payload = " + JsonOutput.prettyPrint(JsonOutput.toJson(oneShopTrans));

oneShopTrans.setUnit_price(oneShopTrans.getUnit_price() * 1.2);

// println "========> I'm in 'testGroovyTransformer01' :: ShopTransaction Object after multipling Price by 20% = " + JsonOutput.prettyPrint(JsonOutput.toJson(oneShopTrans));

println "========> I'm in 'testGroovyTransformer01' :: JSON = " + JsonOutput.toJson(oneShopTrans);

// return JsonOutput.prettyPrint(JsonOutput.toJson(oneShopTrans));

return JsonOutput.toJson(oneShopTrans);

// return payload;
 