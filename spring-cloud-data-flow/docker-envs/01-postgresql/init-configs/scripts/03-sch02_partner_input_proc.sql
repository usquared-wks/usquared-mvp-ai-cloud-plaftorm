
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', 'sch02_partner_input_proc', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


\connect -reuse-previous=on "dbname='DBUSquared'"



CREATE SEQUENCE sch02_partner_input_proc.tb_result_stream01_test_sq_internal_seq
    START 1;

ALTER SEQUENCE sch02_partner_input_proc.tb_result_stream01_test_sq_internal_seq
    OWNER TO dbamaster01;



-- Table: sch02_partner_input_proc.tb_result_stream01_test

-- DROP TABLE sch02_partner_input_proc.tb_result_stream01_test;

CREATE TABLE sch02_partner_input_proc.tb_result_stream01_test
(
    sq_internal bigint NOT NULL DEFAULT nextval('sch02_partner_input_proc.tb_result_stream01_test_sq_internal_seq'::regclass),
    id bigint,
    consumer_id smallint,
    transaction_time character varying(26) COLLATE pg_catalog."default",
    product_id smallint,
    unit_price double precision,
    quantity smallint,
    advert numeric(3,2),
    promotion numeric(3,2),
    CONSTRAINT pk_result_stream01_test PRIMARY KEY (sq_internal)
)

TABLESPACE pg_default;

ALTER TABLE sch02_partner_input_proc.tb_result_stream01_test
    OWNER to dbamaster01;




SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

