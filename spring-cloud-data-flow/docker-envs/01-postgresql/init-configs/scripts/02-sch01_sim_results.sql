SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', 'sch01_sim_results', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


\connect -reuse-previous=on "dbname='DBUSquared'"





CREATE TABLE sch01_sim_results."batch" (
  "id" SERIAL PRIMARY KEY,
  "createdDate" timestamp NOT NULL DEFAULT (now()),
  "simulation_start" timestamp,
  "simulation_end" timestamp,
  "scenario_id" int
);

CREATE TABLE sch01_sim_results."scenarios" (
  "id" SERIAL PRIMARY KEY,
  "batch_id" int,
  "promotion" float,
  "advert" float,
  "price_change" float,
  "market_share" float,
  "revenue" float,
  "settle_down" float
);

CREATE TABLE sch01_sim_results."consumers" (
  "id" SERIAL PRIMARY KEY
);

CREATE TABLE sch01_sim_results."brands" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar
);

CREATE TABLE sch01_sim_results."items" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "price" float,
  "brand_id" int
);

CREATE TABLE sch01_sim_results."brand_actions" (
  "id" SERIAL PRIMARY KEY,
  "iterations" int,
  "scenario_id" int,
  "brand_id" int,
  "marketing" float,
  "advert" float,
  "price_change" float,
  "revenue" float
);

CREATE TABLE sch01_sim_results."consumer_actions" (
  "id" SERIAL PRIMARY KEY,
  "scenario_id" int,
  "consumer_id" int,
  "item_id" int,
  "iterations" int,
  "price" float,
  "quantity" int
);

ALTER TABLE sch01_sim_results."scenarios" ADD FOREIGN KEY ("batch_id") REFERENCES sch01_sim_results."batch" ("id");

ALTER TABLE sch01_sim_results."items" ADD FOREIGN KEY ("brand_id") REFERENCES sch01_sim_results."brands" ("id");

ALTER TABLE sch01_sim_results."brand_actions" ADD FOREIGN KEY ("scenario_id") REFERENCES sch01_sim_results."scenarios" ("id");

ALTER TABLE sch01_sim_results."brand_actions" ADD FOREIGN KEY ("brand_id") REFERENCES sch01_sim_results."brands" ("id");

ALTER TABLE sch01_sim_results."consumer_actions" ADD FOREIGN KEY ("consumer_id") REFERENCES sch01_sim_results."consumers" ("id");

ALTER TABLE sch01_sim_results."consumer_actions" ADD FOREIGN KEY ("item_id") REFERENCES sch01_sim_results."items" ("id");

ALTER TABLE sch01_sim_results."consumer_actions" ADD FOREIGN KEY ("scenario_id") REFERENCES sch01_sim_results."scenarios" ("id");

CREATE INDEX ON sch01_sim_results."batch" ("simulation_start");

CREATE INDEX ON sch01_sim_results."scenarios" ("batch_id");

CREATE INDEX ON sch01_sim_results."scenarios" ("promotion");

CREATE INDEX ON sch01_sim_results."scenarios" ("advert");

CREATE INDEX ON sch01_sim_results."scenarios" ("price_change");

CREATE INDEX ON sch01_sim_results."scenarios" ("market_share");

CREATE INDEX ON sch01_sim_results."scenarios" ("revenue");

CREATE INDEX ON sch01_sim_results."scenarios" ("settle_down");

CREATE INDEX ON sch01_sim_results."items" ("id");

CREATE INDEX ON sch01_sim_results."items" ("brand_id");

CREATE INDEX ON sch01_sim_results."brand_actions" ("scenario_id");

CREATE INDEX ON sch01_sim_results."brand_actions" ("iterations", "scenario_id");

CREATE INDEX ON sch01_sim_results."consumer_actions" ("scenario_id");

CREATE INDEX ON sch01_sim_results."consumer_actions" ("scenario_id", "iterations");





SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;


