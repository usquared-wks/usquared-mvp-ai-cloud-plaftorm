SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

-- DROP DATABASE IF EXISTS "DBUSquared";

--
-- Name: DBECargoWare; Type: DATABASE; Schema: -; Owner: dbamaster01
--

-- CREATE DATABASE "DBUSquared" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C.UTF-8' LC_CTYPE = 'C.UTF-8';


-- ALTER DATABASE "DBUSquared" OWNER TO dbamaster01;


\connect -reuse-previous=on "dbname='DBUSquared'"


CREATE SCHEMA sch00_scdf;

CREATE SCHEMA sch01_sim_results;

CREATE SCHEMA sch02_partner_input_proc;

CREATE SCHEMA sch99_rawdata_tmp;




SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

