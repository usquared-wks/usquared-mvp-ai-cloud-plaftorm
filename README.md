# [To Be Done]

## ...

This is ...


## Tech Stack :

... 

.. 

- Git 2.25.1
- Java 11;
- Maven 3.6.3;
- Spring Boot 2.3.8;
- Spring Utilities: Spring Data JPA + Spring Security
- Lombok;
- MapStruct;
- Swagger + OpenAPI;
- Node.js + NPM + Webpack
- ReactJS
- Docker & Docker-Compose;
- PostgreSQL
- PGAdmin 4;
- KeyCloak (WebSSO)
- Alguma Java IDE (Pode ser o [IntelliJ Idea](https://www.jetbrains.com/idea/) ou o [Eclipse STS - Spring Tool Suite](https://spring.io/tools));
- Alguma IDE para JavaScript (Sugiro o [Visual Studio Code](https://visualstudio.microsoft.com/pt-br/downloads/)) 
- Postman


## Implementation Details:

### Pré-Requisitos:

Tenha certeza que você instalou os seguintes pré-requisitos:

- Git 2.25+
- Java JDK 11 (recomendado: [AdoptOpenJDK](https://adoptopenjdk.net/installation.html))
- Maven 3.6.3+
- Docker 19+
- Docker-Compose 1.27+ 
- Node + NPM (Recomendado usar o [NVM - NPM Version Manager](https://github.com/nvm-sh/nvm/blob/master/README.md)
- Alguma IDE Java + Maven (Optional)
- Alguma IDE JavaScript (Optional)
- Postman (Optional)


### Como Executar a Aplicação:

Efetue o `Git Clone do Projeto`:

- `git clone https://scout23DF@bitbucket.org/monicet-admin/monicetnp-fullstack.git`

Antes de executar a aplicação "monicetnp-fullstack", alguns serviços de infraestrutura devem já estar rodando sem erros. Veja como fazê-los funcionarem corretamente neste [arquivo README.md](./infra-as-code/docker-envs/README.md).

Depois de iniciar pelos menos os serviços de infraestrutura básicos, veja como construir e executar a aplicação "monicetnp-fullstack" neste [arquivo README.md](./monolith-app/README.md).



