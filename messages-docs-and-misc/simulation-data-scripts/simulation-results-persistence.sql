CREATE TABLE "batch" (
  "id" SERIAL PRIMARY KEY,
  "createdDate" timestamp NOT NULL DEFAULT (now()),
  "simulation_start" timestamp,
  "simulation_end" timestamp,
  "scenario_id" int
);

CREATE TABLE "scenarios" (
  "id" SERIAL PRIMARY KEY,
  "batch_id" int,
  "promotion" float,
  "advert" float,
  "price_change" float,
  "market_share" float,
  "revenue" float,
  "settle_down" float
);

CREATE TABLE "consumers" (
  "id" SERIAL PRIMARY KEY
);

CREATE TABLE "brands" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar
);

CREATE TABLE "items" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "price" float,
  "brand_id" int
);

CREATE TABLE "brand_actions" (
  "id" SERIAL PRIMARY KEY,
  "iterations" int,
  "scenario_id" int,
  "brand_id" int,
  "marketing" float,
  "advert" float,
  "price_change" float,
  "revenue" float
);

CREATE TABLE "consumer_actions" (
  "id" SERIAL PRIMARY KEY,
  "scenario_id" int,
  "consumer_id" int,
  "item_id" int,
  "iterations" int,
  "price" float,
  "quantity" int
);

ALTER TABLE "scenarios" ADD FOREIGN KEY ("batch_id") REFERENCES "batch" ("id");

ALTER TABLE "items" ADD FOREIGN KEY ("brand_id") REFERENCES "brands" ("id");

ALTER TABLE "brand_actions" ADD FOREIGN KEY ("scenario_id") REFERENCES "scenarios" ("id");

ALTER TABLE "brand_actions" ADD FOREIGN KEY ("brand_id") REFERENCES "brands" ("id");

ALTER TABLE "consumer_actions" ADD FOREIGN KEY ("consumer_id") REFERENCES "consumers" ("id");

ALTER TABLE "consumer_actions" ADD FOREIGN KEY ("item_id") REFERENCES "items" ("id");

ALTER TABLE "consumer_actions" ADD FOREIGN KEY ("scenario_id") REFERENCES "scenarios" ("id");

CREATE INDEX ON "batch" ("simulation_start");

CREATE INDEX ON "scenarios" ("batch_id");

CREATE INDEX ON "scenarios" ("promotion");

CREATE INDEX ON "scenarios" ("advert");

CREATE INDEX ON "scenarios" ("price_change");

CREATE INDEX ON "scenarios" ("market_share");

CREATE INDEX ON "scenarios" ("revenue");

CREATE INDEX ON "scenarios" ("settle_down");

CREATE INDEX ON "items" ("id");

CREATE INDEX ON "items" ("brand_id");

CREATE INDEX ON "brand_actions" ("scenario_id");

CREATE INDEX ON "brand_actions" ("iterations", "scenario_id");

CREATE INDEX ON "consumer_actions" ("scenario_id");

CREATE INDEX ON "consumer_actions" ("scenario_id", "iterations");
